﻿using System;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SignToolEx.Domain;

namespace SignToolEx.Tests
{
    [TestClass]
    public class TestArgumentsProcessing
    {
        [TestMethod]
        public void ParseCommandLineLongTest()
        {
            var arguments =
                "sign~/a~/tr~http://timestamp.geotrust.com/tsa~/td~sha256~/fd~sha256~/v~/f~\"C:\\mypfxfile.pfx\"~/p~\"WrongPassword\"~/v~\"c:\\thegeekstuff.exe\"";
            var preparedArguments = arguments.Split("~".ToCharArray());
            var parser = new ParamSetParser();
            var first  = parser.Parse(preparedArguments);

            Assert.IsTrue(first.Auto.Value);
            Assert.IsTrue(first.Verbose.Value);
            Assert.AreEqual("\"WrongPassword\"", first.Password);
            Assert.AreEqual("http://timestamp.geotrust.com/tsa",first.TimeStampServerRfcUrl);
            Assert.AreEqual("sha256", first.TimestampDigest);
            Assert.AreEqual("\"C:\\mypfxfile.pfx\"", first.CertificateFile);
            Assert.AreEqual("\"c:\\thegeekstuff.exe\"", first.Files.First());

            Assert.IsNotNull(first);
        }

        [TestMethod]
        public void ParseCommandLineShortTest()
        {
            var arguments = @"sign~/a~/t~http://timestamp.verisign.com/scripts/timstamp.dll~/v~""c:\thegeekstuff.exe""";
            var preparedArguments = arguments.Split("~".ToCharArray());
            var parser = new ParamSetParser();
            var first = parser.Parse(preparedArguments);

            Assert.IsTrue(first.Auto.Value);
            Assert.IsTrue(first.Verbose.Value);
            Assert.AreEqual("http://timestamp.verisign.com/scripts/timstamp.dll",first.TimeStampServerUrl);
            Assert.AreEqual("\"c:\\thegeekstuff.exe\"", first.Files.First());

            Assert.IsNotNull(first);
        }

        [TestMethod]
        public void TestPipes()
        {
            var testData = "Some test string";
            var pipesService = new NamedPipesService();
            var receivedData = string.Empty;
            pipesService.DataReceived += data => receivedData = data;

            try
            {
                pipesService.StartServer();
                pipesService.SendDataToServer(testData);
                Thread.Sleep(300);
                pipesService.StopServer();

                Assert.AreEqual(testData, receivedData);
            }
            catch
            {
                pipesService.StopServer();
                throw;
            }
            finally
            {
                pipesService.DisposeServer();
            }
        }
    }
}
