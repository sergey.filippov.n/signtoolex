﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SignToolEx.Domain;
using SignToolEx.Helpers;

namespace SignToolEx
{
    public class SignToolService
    {
        private ParamSetParser parser;

        private ConcurrentQueue<ParamSet> executionQueue;

        public SignToolService(NamedPipesService namedPipesService)
        {
            namedPipesService.DataReceived += NamedPipesService_DataReceived;
            this.parser = new ParamSetParser();
            this.executionQueue = new ConcurrentQueue<ParamSet>();
        }

        public void Init(string nativeData)
        {
            this.NamedPipesService_DataReceived(nativeData);

            while (!executionQueue.IsEmpty)
            {
                //var awaiter = Task.Factory.StartNew(async () =>
                //{
                //    await Task.Delay(ConfigurationHelper.RepeatDelayMilliseconds);
                //});

                //awaiter.Wait();

                var task = Task.Delay(ConfigurationHelper.RepeatDelayMilliseconds);
                task.ConfigureAwait(false);
                task.Wait();

                ParamSet processedItem=null;
                for (int i = 0; i < 5; i++)
                {
                    if (executionQueue.TryDequeue(out processedItem))
                    {
                        break;
                    }
                }

                if (processedItem == null)
                {
                    continue;
                }

                var preparedCommandLine = string.Join(" ", processedItem.OriginalArgs) + " " + string.Join(" ", processedItem.Files);
                Console.WriteLine(preparedCommandLine);
            }
        }

        private void NamedPipesService_DataReceived(string data)
        {
            var preparedArgs = data.Split(ConfigurationHelper.Separator);
            var newSet = this.parser.Parse(preparedArgs);

            if (newSet.Files == null || newSet.Files.Count == 0)
            {
                throw new ApplicationException("No files were provided by the command line: " + string.Join(" ", preparedArgs));
            }

            var existingSet = executionQueue.FirstOrDefault(item => item.Equals(newSet));
            if (existingSet != null)
            {
                foreach (var newSetFile in newSet.Files)
                {
                    existingSet.Files.Add(newSetFile);
                }
            }
            else
            {
                executionQueue.Enqueue(newSet);
            }
        }
    }
}