﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SignToolEx.Helpers;

namespace SignToolEx
{
    class Program
    {
        public static Mutex mutex;

        static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                PrintHeader();
                ShowUsage();
            }
            else
            {
                var namedPipesService = new NamedPipesService();
                var isOnlyInstance = RunningInstanceCheck(args, namedPipesService);
                if (!isOnlyInstance)
                {
                    return 0;
                }

                PrintHeader();

                try
                {
                    namedPipesService.StartServer();
                    var signingService = new SignToolService(namedPipesService);
                    var preparedData = string.Join(ConfigurationHelper.Separator.ToString(), args);
                    signingService.Init(preparedData);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    namedPipesService.StopServer();
                    return 1;
                }
                finally
                {
                    namedPipesService.DisposeServer();
                }
            }

#if DEBUG
            //Console.WriteLine("Press any key to exit...");
            //Console.ReadKey();
#endif

            return 0;
        }

        public static bool RunningInstanceCheck(string[] args, NamedPipesService namedPipesService)
        {
            mutex = new Mutex(true, ConfigurationHelper.PipeName,out var isSingleRunningInstance);
            if (!isSingleRunningInstance)
            {
                var preparedData = string.Join(ConfigurationHelper.Separator.ToString(), args);
                namedPipesService.SendDataToServer(preparedData);
                return false;
            }

            return true;
        }

        private static void PrintHeader()
        {
            Console.WriteLine("Sing tool extended. 2020. v 1.0");
            Console.WriteLine();
        }

        private static void ShowUsage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("Just apply usual signtool.exe command line parameters. This app will collect all files to be signed within 15 seconds and will sign them all at once.");
        }
    }
}
