﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.Threading.Tasks;
using SignToolEx.Helpers;

namespace SignToolEx
{
    public sealed class NamedPipesService
    {
        private CancellationTokenSource tokenSource;
        public event Action<string> DataReceived;

        public void StartServer()
        {
            tokenSource = new CancellationTokenSource();
            var cancellationToken = tokenSource.Token;

            var task = Task.Factory.StartNew(() =>
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return;
                }

                while (true)
                {
                    string data;
                    using (var server = new NamedPipeServerStream(ConfigurationHelper.PipeName))
                    {
                        server.WaitForConnection();

                        using (var reader = new StreamReader(server))
                        {
                            data = reader.ReadToEnd();
                        }
                    }

                    this.OnDataReceived(data);

                    if (cancellationToken.IsCancellationRequested)
                    {
                        break;
                    }
                }

            }, cancellationToken);
        }

        public void StopServer()
        {
            if (tokenSource == null)
            {
                return;
            }

            if (tokenSource.Token.IsCancellationRequested)
            {
                return;
            }

            tokenSource.Cancel();
        }

        public bool SendDataToServer(string data, int connectionTimeout = 2000)
        {
            using (var client = new NamedPipeClientStream(ConfigurationHelper.PipeName))
            {
                try
                {
                    client.Connect(connectionTimeout);
                }
                catch (Exception exception)
                {
                    return false;
                }

                if (!client.IsConnected)
                {
                    return false;
                }

                using (var writer = new StreamWriter(client))
                {
                    writer.Write(data);
                    writer.Flush();
                }
            }

            return true;
        }

        public void DisposeServer()
        {
            tokenSource?.Dispose();
            tokenSource = null;
        }

        private void OnDataReceived(string data)
        {
            this.DataReceived?.Invoke(data);
        }
    }
}