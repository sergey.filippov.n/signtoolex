﻿using System;
using System.Text;

namespace SignToolEx.Domain
{
    public class ParamSetParser
    {
        public ParamSetParser()
        {

        }

        public ParamSet Parse(string[] arguments)
        {
            if (arguments == null || arguments.Length == 0)
            {
                throw new ApplicationException("Cannot parse empty arguments.");
            }

            if (!string.Equals("sign", arguments[0], StringComparison.OrdinalIgnoreCase))
            {
                throw new ApplicationException("Only 'sing' command of 'signtool.exe' is supported.");
            }

            var result = new ParamSet();

            var fileStartIndex = -1;

            for (int index = 1; index < arguments.Length; index++)
            {
                var part = arguments[index];
                if (part.StartsWith("/") || part.StartsWith("-"))
                {
                    var skipNext = ProcessOption(part, arguments[index + 1], result);
                    if (skipNext)
                    {
                        index += 1;
                        continue;
                    }
                }
                else
                {
                    if (fileStartIndex == -1)
                    {
                        fileStartIndex = index;
                    }

                    var preparedFile = part.Trim().Trim('"');

                    result.Files.Add("\"" + preparedFile + "\"");
                }
            }

            result.OriginalArgs = new string[fileStartIndex];
            Array.Copy(arguments, result.OriginalArgs, fileStartIndex);

            return result;
        }

        private bool ProcessOption(string name, string nextValue, ParamSet result)
        {
            var preparedName = name.ToLower().TrimStart("/-".ToCharArray()).Trim();
            var isSkip = false;
            switch (preparedName)
            {
                case "q":
                    result.Queit = true; break;
                case "v":
                    result.Verbose = true; break;
                case "debug":
                    result.Debug = true; break;
                case "a":
                    result.Auto = true; break;
                case "ac":
                    result.AutoCertificate = nextValue;
                    isSkip = true; break;
                case "as":
                    result.AutoSignature = true; break;
                case "c":
                    result.CertificateTemplate = nextValue;
                    isSkip = true; break;
                case "csp":
                    result.CryptographicServiceProvider = nextValue;
                    isSkip = true; break;
                case "d":
                    result.Description = nextValue;
                    isSkip = true; break;
                case "du":
                    result.DescriptionUrl = nextValue;
                    isSkip = true; break;
                case "f":
                    result.CertificateFile = nextValue;
                    isSkip = true; break;
                case "fd":
                    result.FileDigest = nextValue;
                    isSkip = true; break;
                case "i":
                    result.IssuerName = nextValue;
                    isSkip = true; break;
                case "kc":
                    result.KeyContainer = nextValue;
                    isSkip = true; break;
                case "n":
                    result.SubjectName = nextValue;
                    isSkip = true; break;
                case "nph":
                    result.NoPageHashes = true; break;
                case "p":
                    result.Password = nextValue;
                    isSkip = true; break;
                case "p7":
                    result.ProducePkcs7Path = nextValue;
                    isSkip = true; break;
                case "p7ce":
                    result.Pkcs7ContentEmbeding = nextValue;
                    isSkip = true; break;
                case "p7co":
                    result.PkcsObjectIdentifier = nextValue;
                    isSkip = true; break;
                case "ph":
                    result.PageHashes = true; break;
                case "r":
                    result.RootSubjectName = nextValue;
                    isSkip = true; break;
                case "s":
                    result.StoreName = nextValue;
                    isSkip = true; break;
                case "sha1":
                    result.Sha1Hash = nextValue;
                    isSkip = true; break;
                case "sm":
                    result.MachineStore = true; break;
                case "t":
                    result.TimeStampServerUrl = nextValue;
                    isSkip = true; break;
                case "td":
                    result.TimestampDigest = nextValue;
                    isSkip = true; break;
                case "tr":
                    result.TimeStampServerRfcUrl = nextValue;
                    isSkip = true; break;
                case "u":
                    result.KeyUsage = nextValue;
                    isSkip = true; break;
                case "uw ":
                    result.KeyUsageWindows = true; break;
                default: throw new ApplicationException("Command line switch is not supported: " + name);
            }

            return isSkip;
        }
    }
}