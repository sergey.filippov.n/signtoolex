﻿using System;
using System.Collections.Generic;

namespace SignToolEx.Domain
{
    public class ParamSet
    {
        public ParamSet()
        {
            this.Files = new HashSet<string>();
        }

        #region GenericSwithces

        // Displays no output if the command runs successfully, and displays minimal output if the command fails. [/q]
        public bool? Queit { get; set; }

        // Displays verbose output regardless of whether the command runs successfully or fails, and displays warning messages. [/v]
        public bool? Verbose { get; set; }

        // Displays debugging information. [/debug]
        public bool? Debug { get; set; }

        #endregion GenericSwithces

        #region SignSwitches

        // Automatically selects the best signing certificate. [/a]
        public bool? Auto { get; set; }

        // Adds an additional certificate from file to the signature block. [/ac <file>]
        public string AutoCertificate { get; set; }

        // Appends this signature. If no primary signature is present, this signature is made the primary signature instead. [/as]
        public bool? AutoSignature { get; set; }

        // Specifies the Certificate Template Name (a Microsoft extension) for the signing certificate. [/c CertTemplateName]
        public string CertificateTemplate { get; set; }

        // Specifies the cryptographic service provider (CSP) that contains the private key container. [/csp CSPName]
        public string CryptographicServiceProvider { get; set; }

        // Specifies a description of the signed content. [/d desc]
        public string Description { get; set; }

        // Specifies a Uniform Resource Locator (URL) for the expanded description of the signed content. [/du URL]
        public string DescriptionUrl { get; set; }

        // Specifies the signing certificate in a file. [/f CertFile]
        public string CertificateFile { get; set; }

        // Specifies the file digest algorithm to use for creating file signatures. The default is SHA1. [/fd]
        public string FileDigest { get; set; }

        // Specifies the name of the issuer of the signing certificate. [/i IssuerName]
        public string IssuerName { get; set; }

        // Specifies the private key container name. [/kc PrivKeyContainerName]
        public string KeyContainer { get; set; }

        // Specifies the name of the subject of the signing certificate. [/n SubjName]
        public string SubjectName { get; set; }

        // If supported, suppresses page hashes for executable files.
        // The default is determined by the SIGNTOOL_PAGE_HASHES environment variable and by the wintrust.dll version.
        // This option is ignored for non-PE files. [/nph]
        public bool? NoPageHashes { get; set; }

        // Specifies the password to use when opening a PFX file. (Use the /f option to specify a PFX file.) [/p pass]
        public string Password { get; set; }

        // Specifies that a Public Key Cryptography Standards (PKCS) #7 file is produced for each specified content file. [/p7 Path]
        public string ProducePkcs7Path { get; set; }

        // Specifies options for the signed PKCS #7 content. [/p7ce Value]
        public string Pkcs7ContentEmbeding { get; set; }

        // Specifies the object identifier (OID) that identifies the signed PKCS #7 content. [/p7co OID]
        public string PkcsObjectIdentifier { get; set; }

        // If supported, generates page hashes for executable files. [/ph]
        public bool? PageHashes { get; set; }

        // Specifies the name of the subject of the root certificate that the signing certificate must chain to. [/r RootSubjName]
        public string RootSubjectName { get; set; }

        // Specifies the store to open when searching for the certificate. [/s StoreName]
        public string StoreName { get; set; }

        // Specifies the SHA1 hash of the signing certificate. [/sha1 hash]
        public string Sha1Hash { get; set; }

        // Specifies that a machine store, instead of a user store, is used. [/sm]
        public bool? MachineStore { get; set; }

        // Specifies the URL of the time stamp server. [/t URL]
        public string TimeStampServerUrl { get; set; }

        // Used with the /tr option to request a digest algorithm used by the RFC 3161 time stamp server. [/td alg]
        public string TimestampDigest { get; set; }

        // Specifies the URL of the RFC 3161 time stamp server. [/tr URL]
        public string TimeStampServerRfcUrl { get; set; }

        // Specifies the enhanced key usage (EKU) that must be present in the signing certificate. [/u Usage]
        public string KeyUsage { get; set; }

        // Specifies usage of "Windows System Component Verification". [/uw]
        public bool? KeyUsageWindows { get; set; }

        #endregion SignSwitches

        public HashSet<string> Files { get; set; }

        public string[] OriginalArgs { get; set; }

        public ParamSet Clone()
        {
            var result = new ParamSet
            {
                Queit = this.Queit,
                Verbose = this.Verbose,
                Debug = this.Debug,
                Auto = this.Auto,
                AutoCertificate = this.AutoCertificate,
                AutoSignature = this.AutoSignature,
                CertificateTemplate = this.CertificateTemplate,
                CryptographicServiceProvider = this.CryptographicServiceProvider,
                Description = this.Description,
                DescriptionUrl = this.DescriptionUrl,
                CertificateFile = this.CertificateFile,
                FileDigest = this.FileDigest,
                IssuerName = this.IssuerName,
                KeyContainer = this.KeyContainer,
                SubjectName = this.SubjectName,
                NoPageHashes = this.NoPageHashes,
                Password = this.Password,
                ProducePkcs7Path = this.ProducePkcs7Path,
                Pkcs7ContentEmbeding = this.Pkcs7ContentEmbeding,
                PkcsObjectIdentifier = this.PkcsObjectIdentifier,
                PageHashes = this.PageHashes,
                RootSubjectName = this.RootSubjectName,
                StoreName = this.StoreName,
                Sha1Hash = this.Sha1Hash,
                MachineStore = this.MachineStore,
                TimeStampServerUrl = this.TimeStampServerUrl,
                TimestampDigest = this.TimestampDigest,
                TimeStampServerRfcUrl = this.TimeStampServerRfcUrl,
                KeyUsage = this.KeyUsage,
                KeyUsageWindows = this.KeyUsageWindows,
                Files = new HashSet<string>(this.Files)
            };


            if (this.OriginalArgs != null)
            {
                result.OriginalArgs = new string[this.OriginalArgs.Length];
                Array.Copy(this.OriginalArgs, result.OriginalArgs, this.OriginalArgs.Length);
            }

            return result;
        }

        protected bool Equals(ParamSet other)
        {
            return Queit == other.Queit && Verbose == other.Verbose && Debug == other.Debug && Auto == other.Auto && AutoCertificate == other.AutoCertificate && AutoSignature == other.AutoSignature && CertificateTemplate == other.CertificateTemplate && CryptographicServiceProvider == other.CryptographicServiceProvider && Description == other.Description && DescriptionUrl == other.DescriptionUrl && CertificateFile == other.CertificateFile && FileDigest == other.FileDigest && IssuerName == other.IssuerName && KeyContainer == other.KeyContainer && SubjectName == other.SubjectName && NoPageHashes == other.NoPageHashes && Password == other.Password && ProducePkcs7Path == other.ProducePkcs7Path && Pkcs7ContentEmbeding == other.Pkcs7ContentEmbeding && PkcsObjectIdentifier == other.PkcsObjectIdentifier && PageHashes == other.PageHashes && RootSubjectName == other.RootSubjectName && StoreName == other.StoreName && Sha1Hash == other.Sha1Hash && MachineStore == other.MachineStore && TimeStampServerUrl == other.TimeStampServerUrl && TimestampDigest == other.TimestampDigest && TimeStampServerRfcUrl == other.TimeStampServerRfcUrl && KeyUsage == other.KeyUsage && KeyUsageWindows == other.KeyUsageWindows;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ParamSet) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Queit.GetHashCode();
                hashCode = (hashCode * 397) ^ Verbose.GetHashCode();
                hashCode = (hashCode * 397) ^ Debug.GetHashCode();
                hashCode = (hashCode * 397) ^ Auto.GetHashCode();
                hashCode = (hashCode * 397) ^ (AutoCertificate != null ? AutoCertificate.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ AutoSignature.GetHashCode();
                hashCode = (hashCode * 397) ^ (CertificateTemplate != null ? CertificateTemplate.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (CryptographicServiceProvider != null ? CryptographicServiceProvider.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Description != null ? Description.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (DescriptionUrl != null ? DescriptionUrl.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (CertificateFile != null ? CertificateFile.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (FileDigest != null ? FileDigest.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (IssuerName != null ? IssuerName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (KeyContainer != null ? KeyContainer.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (SubjectName != null ? SubjectName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ NoPageHashes.GetHashCode();
                hashCode = (hashCode * 397) ^ (Password != null ? Password.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ProducePkcs7Path != null ? ProducePkcs7Path.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Pkcs7ContentEmbeding != null ? Pkcs7ContentEmbeding.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PkcsObjectIdentifier != null ? PkcsObjectIdentifier.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ PageHashes.GetHashCode();
                hashCode = (hashCode * 397) ^ (RootSubjectName != null ? RootSubjectName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (StoreName != null ? StoreName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Sha1Hash != null ? Sha1Hash.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ MachineStore.GetHashCode();
                hashCode = (hashCode * 397) ^ (TimeStampServerUrl != null ? TimeStampServerUrl.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (TimestampDigest != null ? TimestampDigest.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (TimeStampServerRfcUrl != null ? TimeStampServerRfcUrl.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (KeyUsage != null ? KeyUsage.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ KeyUsageWindows.GetHashCode();
                return hashCode;
            }
        }
    }
}