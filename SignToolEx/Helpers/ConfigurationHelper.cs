﻿namespace SignToolEx.Helpers
{
    public static class ConfigurationHelper
    {
        public const char Separator = '~';

        public const string PipeName = "ExtendedSignTool";

        public const int RepeatDelayMilliseconds = 5000;
    }
}